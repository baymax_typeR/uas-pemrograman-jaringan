from django.contrib import admin
from .models import UAS

# Register your models here.
@admin.register(UAS)
class UASAdmin(admin.ModelAdmin):
    list_display = ['nik','nama','ttl','alamat','usia','jk','agama','hp','pendidikan','status','riwayat','posisi','tgl_daftar']
    list_filter = ['jk','pendidikan','status','posisi']
    search_fields = ['nik','nama','ttl','alamat','usia','jk','agama','hp','pendidikan','status','riwayat','posisi','tgl_daftar']