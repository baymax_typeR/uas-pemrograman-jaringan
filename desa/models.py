from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone
# Create your models here.

class UAS(models.Model):
	JK_CHOICES = (('lk', 'Laki-laki'), ('pr', 'Perempuan'))
	POSISI_CHOICES = (('sk', 'Sekretaris Desa'), ('tu', 'Kaur Tata Usaha dan Umum'), ('ug', 'Kaur Keuangan'), ('rc', 'Kaur Perencanaan'), ('pm', 'Kasi Pemerintahan'), ('ks', 'Kasi Kesejahteraan'), ('pl', 'Kasi Pelayanan'))
	STATUS_CHOICES = (('bm', 'Belum Menikah'), ('mk', 'Menikah'), ('cr', 'Cerai'))
	PENDIDIKAN_CHOICES = (('sd', 'SD'), ('sp', 'SMP'), ('sa', 'SMA'), ('dp', 'Diploma'), ('sr', 'Sarjana'), ('mg', 'Magister'))    
	nik = models.CharField('NIK', max_length=50, null=False)
	nama = models.CharField('Nama Lengkap', max_length=50, null=False)
	ttl = models.DateTimeField('Tgl Lahir', default=timezone.now)
	alamat = models.CharField('Alamat', max_length=50, null=False)
	usia = models.FloatField('Usia')
	jk = models.CharField('Jenis Kelamin', max_length=2, choices=JK_CHOICES)
	agama = models.CharField('Agama', max_length=50, null=False)
	hp = models.CharField('No. HP', max_length=50, null=False)
	pendidikan = models.CharField('Pendidikan Terakhir', max_length=2, choices=PENDIDIKAN_CHOICES)
	status = models.CharField('Status', max_length=2, choices=STATUS_CHOICES)
	riwayat = models.CharField('Riwayat Penyakit', max_length=100, null=False)
	posisi = models.CharField('Posisi yang Dilamar', max_length=2, choices=POSISI_CHOICES)
	tgl_daftar = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ['-tgl_daftar']

	def __str__(self):
		return self.nik

	def get_absolute_url(self):
		return reverse('home_page')

