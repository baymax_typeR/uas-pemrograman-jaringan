from django.urls import path
from .views import index, AlatDetailView, AlatCreateView, AlatDeleteView, AlatEditView

urlpatterns = [    
    path('', index, name="home_page"),
    path('desa/<int:pk>', AlatDetailView.as_view(), name='alat_detail_view'),
    path('desa/delete/<int:pk>', AlatDeleteView.as_view(), name='alat_delete'),    
    path('desa/edit/<int:pk>', AlatEditView.as_view(), name='alat_edit'),    
    path('daftar', AlatCreateView.as_view(), name="alat_add")
]