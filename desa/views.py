from .models import UAS
from django.shortcuts import render
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# Create your views here.

var = {
	'judul' : 'Sistem Informasi Pendaftaran Perangkat Desa Karanggondang Berbasis Website',
	'info' : '''Ini adalah website untuk pendaftaran calon perangkat Desa Karanggondang''',
	'oleh' : 'Fulan'
}

def index(self):
    var['uas'] = UAS.objects.values('id','nik','nama','ttl','alamat','usia','jk','agama','hp','pendidikan','status','riwayat','posisi','tgl_daftar').\
        order_by('nik')
    return render(self, 'desa/index.html', context=var)

class AlatDetailView(DetailView):
    model = UAS
    template_name = 'desa/alat_detail_view.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AlatCreateView(CreateView):
    model = UAS
    fields = '__all__'
    template_name = 'desa/alat_add.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AlatEditView(UpdateView):
    model = UAS
    fields = ['nik','nama','ttl','alamat','usia','jk','agama','hp','pendidikan','status','riwayat','posisi']
    template_name = 'desa/alat_edit.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AlatDeleteView(DeleteView):
    model = UAS
    template_name = 'desa/alat_delete.html'
    success_url = reverse_lazy('home_page')

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

